/**
  * @file GroundWaterNitrogen.cpp
  * @author ...
  * ...
  */

/*
 * @@tagdynamic@@
 * @@tagdepends: vle.discrete-time @@endtagdepends
*/

#include <vle/DiscreteTime.hpp>

namespace vd = vle::devs;

namespace vv = vle::value;

namespace GroundWaterNitrogen {

using namespace vle::discrete_time;

class GroundWaterNitrogen : public DiscreteTimeDyn
{
public:
GroundWaterNitrogen(
    const vd::DynamicsInit& init,
    const vd::InitEventList& evts)
        : DiscreteTimeDyn(init, evts)
{
    NStock.init(this, "NStock", evts);
    concirr.init(this, "concirr", evts);
    Nconc.init(this, "Nconc", evts);
    NLeaching.init(this, "NLeaching", evts);
    NIrrigation.init(this, "NIrrigation", evts);
    NDischarge.init(this, "NDischarge", evts);
    lessiv.init(this, "lessiv", evts);
    airg.init(this, "airg", evts);
    GWVol.init(this, "GWVol", evts);
    discharge.init(this, "discharge", evts);
    PlotArea = (evts.exist("PlotArea")) ? vv::toDouble(evts.get("PlotArea")) : 10000.;
    GWpixelArea = (evts.exist("GWpixelArea")) ? vv::toDouble(evts.get("GWpixelArea")) : 10000.;
}

virtual ~GroundWaterNitrogen()
{}

void compute(const vle::devs::Time& t)
{
	NLeaching = lessiv(-1) * PlotArea / 10000.;
    NDischarge = discharge(-1) * GWpixelArea * Nconc(-1);
    Nconc = (GWVol(-1) > 0) ? NStock(-1) / GWVol(-1) : 0.;
    concirr = Nconc() /(PlotArea /10000.) / 10.;
    NIrrigation = airg(-1) * (PlotArea / 1000.) * Nconc();
    NStock = NStock(-1) + NLeaching() - NIrrigation() - NDischarge();
}

    Var NStock;
    Var concirr;
    Var Nconc;
    Var NLeaching;
    Var NIrrigation;
    Var NDischarge;
    Var lessiv;
    Var airg;
    Var GWVol;
    Var discharge;
    double PlotArea;
    double GWpixelArea;

};

} // namespace GroundWaterNitrogen

DECLARE_DYNAMICS(GroundWaterNitrogen::GroundWaterNitrogen)

