
# Package GroundWaterNitrogen for vle-2.0.2

The **GroundWaterNitrogen** package provide a single model called **GroundWaterNitrogen** and corresponding configurations in order to facilitate the use of the model.  
This simple model is simulating the nitrogen content in groundwater following passive water flow and assuming perfect mixing.

# Table of Contents
1. [Package dependencies](#p1)
2. [Atomic model GroundWaterNitrogen](#p2)
    1. [Configuring a GroundWaterNitrogen Model](#p2.1)
        1. [Dynamics settings](#p2.1.1)
        2. [Parameters settings](#p2.1.2)
        3. [Input settings](#p2.1.3)
        4. [Output settings](#p2.1.4)
        5. [Observation settings](#p2.1.5)
        6. [Available configurations](#p2.1.6)
    2. [Details](#p2.2)

---

## Package dependencies <a name="p1"></a>

List of required external packages (with link to distribution when available).

* [vle.discrete-time](http://www.vle-project.org/pub/2.0/vle.discrete-time.tar.bz2)

--- 

## Atomic model GroundWaterNitrogen <a name="p2"></a>

The **GroundWaterNitrogen** model (using extension class [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/)) estimate the dynamics of Nitrogen stock and concentration in a single groundwater voxel (considered as an homogeneous volume, ie. Perfect mixture hypothesis).

<button type='button' class='btn btn-danger' data-toggle='collapse' data-target='#YY_panel'>Show/Hide Model Details</button>
<div id="YY_panel" class="collapse">

### Configuring a GroundWaterNitrogen Model <a name="p2.1"></a>

#### Dynamics settings <a name="p2.1.1"></a>

To define the dynamic:

* **library** : GroundWaterNitrogen
* **name** : GroundWaterNitrogen

#### Parameters settings <a name="p2.1.2"></a>

List and information about all possible parameters (name, type, ismandatory, description with default value).

See documentation of [`DiscreteTimeDyn`](http://www.vle-project.org/packages/vle.discrete-time/) to get all available extension parameters (for example **time_step**)

| Parameter name | Type | Is mandatory? | Description |
| -- | :--: | :--: | -- |
| **PlotArea** | double |[] default:10000 | Plot surface area (m<sup>2</sup>) |
| **GWpixelArea** | double | [] default:10000 | Groundwater voxel surface area (m<sup>2</sup>) |
| **init_value_NStock** | double | [x] | initial NStock value at simulation start (kgN) |

#### Input settings <a name="p2.1.3"></a>

List and information about all possible input ports (name, type, description with units).

for model using extension discrete-time also include sync info 

* **airg :** Daily flow of irrigation water (mm.m<sup>-2</sup>.day<sup>-1</sup>) (double) (nosync)  
* **lessiv :** Daily amount of NO3-N leached at the base of the soil profile (kgN.ha<sup>-1</sup>.day<sup>-1</sup>) (double) (nosync)  
* **discharge : ** Daily groundwater discharge flow (m<sup>3</sup>.m<sup>-2</sup>.day<sup>-1</sup>) (double) (nosync)  
* **GWVol : ** Water volume in the groundwater voxel (m<sup>3</sup>) (double) (nosync)  

#### Output settings <a name="p2.1.4"></a>

List and information about all possible output ports (name, type, description with units).

* **NLeaching :** Daily amount of nitrogen leaching to the  groundwater voxel (kgN.day<sup>-1</sup>) (double)  
* **NDischarge :** Daily amount of Nitrogen discharge from the  groundwater voxel (kgN.day<sup>-1</sup>) (double)  
* **Nconc :** Nitrogen concentration in the groundwater voxel (kgN.m<sup>-3</sup>) (double)  
* **concirr :** Nitrogen concentration in the irrigation water (kgN.ha<sup>-1</sup>.mm<sup>-1</sup>) (double)  
* **NIrrigation :** Nitrogen Stock in the irrigation water (kgN.day<sup>-1</sup>) (double)
* **NStock :** Nitrogen Stock in the groundwater voxel (kgN) (double)  

#### Observation settings <a name="p2.1.5"></a>

List and information about all possible observation ports (name, type, description with units).

* **NLeaching :** Daily amount of nitrogen leaching to the  groundwater voxel (kgN.day<sup>-1</sup>) (double)  
* **NDischarge :** Daily amount of Nitrogen discharge from the  groundwater voxel (kgN.day<sup>-1</sup>) (double)  
* **Nconc :** Nitrogen concentration in the groundwater voxel (kgN.m<sup>-3</sup>) (double)  
* **concirr :** Nitrogen concentration in the irrigation water (kgN.ha<sup>-1</sup>.mm<sup>-1</sup>) (double)  
* **NIrrigation :** Nitrogen Stock in the irrigation water (kgN.day<sup>-1</sup>) (double)
* **NStock :** Nitrogen Stock in the groundwater voxel (kgN) (double)  

#### Available configurations <a name="p2.1.6"></a>

List and information about all configuration metadata files
* GroundWaterNitrogen/GroundWaterNitrogen : set all parameters with default values

### Details <a name="p2.2"></a>

#### State variables equations <a name="p2.2.1"></a>

> $NLeaching(t) = lessiv(t-1) * PlotArea(t) / 10000$

> $NDischarge(t) = discharge(t-1) * GWpixelArea(t) * Nconc(t-1)$

> $Nconc(t) = NStock(t-1) / GWVol(t-1)$ ; if GWVol(t-1) > 0
>
> $Nconc(t) = 0$ ; else

> $concirr(t) = Nconc(t) /(PlotArea(t) /10000) / 10$

> $NIrrigation(t) = airg(t-1) * (PlotArea(t) / 1000) * Nconc(t)$

> $NStock(t) = NStock(t-1) + NLeaching(t) - NIrrigation(t) - NDischarge(t)$

</div>

